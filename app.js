require('dotenv').config();
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');

const express  = require('express');
const app = express();

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);

//Cors
app.use(function(req, res, next) {
    // Website you wish to allow to connect
    var allowedOrigins = [
    ];

    //res.setHeader("Access-Control-Allow-Origin", '*');
    var origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader("Access-Control-Allow-Origin", origin);
    }

    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );

    // Request headers you wish to allow
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin,Accept,X-Requested-With,Content-Type,X-Auth-Token"
    );

    // Set to true if you need the website to
    // include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);

    // intercept OPTIONS method
    if ("OPTIONS" == req.method) {
        res.sendStatus(200);
    } else {
        // Pass to next layer of middleware
        next();
    }
});

//Use routes
// app.use('/api/v1', index);

app.use(express.static(path.join(__dirname, 'xyz-web')));
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'xyz-web', 'index.html'));
});

app.all('*', function(req, res){
    res.status(400)
    res.send('Invalid route');
});

let server;
server = http.createServer(app);
  
/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(process.env.PORT);
server.on('listening', () => {
    console.log("Server started on port:", process.env.PORT)
});