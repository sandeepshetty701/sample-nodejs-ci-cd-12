/**
 * @author mrdoob / http://mrdoob.com/
 * @author Mugen87 / https://github.com/Mugen87
 */

import {
	Euler,
	EventDispatcher,
	Vector3
} from "three";

var PointerLockControls = function ( camera, domElement ) {

	if ( domElement === undefined ) {

		console.warn( 'THREE.PointerLockControls: The second parameter "domElement" is now mandatory.' );
		domElement = document.body;

	}

	this.domElement = domElement;
	this.enabled = false;
	this.name = 'PointerLockControls';

	//
	// internals
	//

	var scope = this;

	var changeEvent = { type: 'change' };
	var lockEvent = { type: 'lock' };
	var unlockEvent = { type: 'unlock' };

	var rotateStart = {x: null,y: null};
	var dollyStart = {x: null,t: null};

	var limits = {up: Math.PI, bottom: 0, right: Infinity, left: -Infinity, fov: 100};

	var euler = new Euler( 0, 0, 0, 'ZYX' );
	// var euler = new Euler( 0, 0, 0);

	var PI_2 = Math.PI / 2;

	var vec = new Vector3();

	this.setLimits = function (type, value) {

		limits[type] = value;

	};

	this.resetLimits = function (type, value) {

		limits = {up: Math.PI, bottom: 0, right: Infinity, left: -Infinity, fov: 100};

	};

	function onTouchStart(event) {
		switch(event.touches.length) {
			case 1:
				rotateStart.x = event.touches[ 0 ].pageX;
				rotateStart.y = event.touches[ 0 ].pageY;
				break;
			case 2:
				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				var distance = Math.sqrt( dx * dx + dy * dy );
				dollyStart.x = 0;
				dollyStart.y = distance;
				break;
		}
		
	}

	function onTouchMove(event) {
		if ( scope.enabled === false ) {
			return;
		} else {
			switch(event.touches.length) {
				case 1: // one-fingered touch: rotate
					let rotateEnd = {x: null,y: null};
					if ( event.touches.length == 1 ) {
						// rotateEnd.set( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
						rotateEnd.x = event.touches[ 0 ].pageX;
						rotateEnd.y = event.touches[ 0 ].pageY;
					} 
					let deltaX = rotateEnd.x - rotateStart.x;
					let deltaY = rotateEnd.y - rotateStart.y;

					rotateStart.x = rotateEnd.x;
					rotateStart.y = rotateEnd.y;

					euler.setFromQuaternion( camera.quaternion );
					euler.z += deltaX * 0.002;
					euler.x -= deltaY * 0.002;

					// euler.x = Math.max( - PI_2, Math.min( PI_2, euler.x ) );

					euler.x = Math.max( 0, Math.min( Math.PI, euler.x ) );

					camera.quaternion.setFromEuler( euler );

					scope.dispatchEvent( changeEvent );
			
					break;
				case 2: // two-fingered touch: dolly
					var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
					var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
					var distance = Math.sqrt( dx * dx + dy * dy );
					let dollyEnd = {x: null,y: null};
					dollyEnd.x = 0;
					dollyEnd.y = distance;

					let dollyDeltaY = dollyEnd.y - dollyStart.y; 

					onTouchWheel({deltaY: dollyDeltaY});
			
					dollyStart.x = dollyEnd.x;
					dollyStart.y = dollyEnd.y;
					
					scope.dispatchEvent( changeEvent );
					break;
			}
		}
	}

	function onMouseMove( event ) {
		scope.dispatchEvent( changeEvent );
		if ( scope.enabled === false ) {
			return;
		} else if (event.which == 1) {
			var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
			var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

			// var movementY = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
			// var movementX = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

			euler.setFromQuaternion( camera.quaternion );
			euler.z += movementX * 0.002;
			euler.x -= movementY * 0.002;

			// euler.x = Math.max( - PI_2, Math.min( PI_2, euler.x ) );

			// euler.x = Math.max( 1 , Math.min( Math.PI, euler.x ) );

			// euler.x = Math.max( 1.034 , Math.min( 1.27, euler.x ) );

			euler.x = Math.max( limits.bottom , Math.min( limits.up, euler.x ) );
			euler.z = Math.max( limits.left , Math.min( limits.right, euler.z ) );

			camera.quaternion.setFromEuler( euler );

			scope.dispatchEvent( changeEvent );
		}
	}

	function onTouchWheel( event ) {
		if ( scope.enabled === false ) {
			return;
		}
		if ( event.deltaY > 0 && camera.fov >= 15) {
			camera.fov = camera.fov - 2;
		} else if ( event.deltaY < 0 && camera.fov <= 95 ) {
			camera.fov = camera.fov + 2;
		}
		camera.updateProjectionMatrix();
		scope.dispatchEvent( changeEvent );
	}

	function onMouseWheel( event ) {
		// event.preventDefault();
		event.stopPropagation();
		if ( scope.enabled === false ) {
			return;
		}
		if ( event.deltaY < 0 && camera.fov >= 15) {
			camera.fov = camera.fov - 5;
		} else if ( event.deltaY > 0 && camera.fov <= limits.fov ) {
			camera.fov = camera.fov + 5;
		}
		camera.updateProjectionMatrix();
		scope.dispatchEvent( changeEvent );
	}

	function onPointerlockError() {

		console.error( 'THREE.PointerLockControls: Unable to use Pointer Lock API' );

	}

	this.connect = function () {

		domElement.addEventListener( 'mousemove', onMouseMove, false );
		domElement.addEventListener( 'touchstart', onTouchStart, false );
		domElement.addEventListener( 'touchmove', onTouchMove, false );
		// domElement.addEventListener( 'pointerlockchange', onPointerlockChange, false );
		// domElement.addEventListener( 'pointerlockerror', onPointerlockError, false );
		domElement.addEventListener( 'wheel', onMouseWheel, false );

	};

	this.disconnect = function () {

		domElement.removeEventListener( 'mousemove', onMouseMove, false );
		domElement.removeEventListener( 'touchmove', onTouchMove, false );
		// domElement.removeEventListener( 'pointerlockchange', onPointerlockChange, false );
		// domElement.removeEventListener( 'pointerlockerror', onPointerlockError, false );

	};

	this.dispose = function () {

		this.disconnect();

	};

	this.getObject = function () { // retaining this method for backward compatibility

		return camera;

	};

	this.getDirection = function () {

		var direction = new Vector3( 0, 0, - 1 );

		return function ( v ) {

			return v.copy( direction ).applyQuaternion( camera.quaternion );

		};

	}();

	this.lock = function () {

		// this.domElement.requestPointerLock();
		scope.enabled = true;

	};

	this.unlock = function () {

		// document.exitPointerLock();
		scope.enabled = false;

	};

	this.connect();

};

PointerLockControls.prototype = Object.create( EventDispatcher.prototype );
PointerLockControls.prototype.constructor = PointerLockControls;

export { PointerLockControls };


// import {
// 	Euler,
// 	EventDispatcher,
// 	Vector3
// } from "../../../build/three.module.js";

// var PointerLockControls = function (camera, domElement) {

// 	var changeEvent = { type: 'change' };
// 	var scope = this;
// 	var isUserInteracting = false,
// 	onMouseDownMouseX = 0, onMouseDownMouseY = 0,
// 	lon = 0, onMouseDownLon = 0,
// 	lat = 0, onMouseDownLat = 0,
// 	phi = 0, theta = 0;

// 	if (domElement === undefined) {

// 		console.warn('THREE.PointerLockControls: The second parameter "domElement" is now mandatory.');
// 		domElement = document.body;

// 	}

// 	domElement.addEventListener('mousedown', onPointerStart, false);
// 	domElement.addEventListener('mousemove', onPointerMove, false);
// 	domElement.addEventListener('mouseup', onPointerUp, false);

// 	domElement.addEventListener('wheel', onDocumentMouseWheel, false);

// 	domElement.addEventListener('touchstart', onPointerStart, false);
// 	domElement.addEventListener('touchmove', onPointerMove, false);
// 	domElement.addEventListener('touchend', onPointerUp, false);

// 	window.addEventListener('resize', onWindowResize, false);

// 	function onWindowResize() {

// 		camera.aspect = window.innerWidth / window.innerHeight;
// 		camera.updateProjectionMatrix();
// 		update();

// 		// renderer.setSize(window.innerWidth, window.innerHeight);

// 	}

// 	function onPointerStart(event) {

// 		isUserInteracting = true;

// 		var clientX = event.clientX || event.touches[0].clientX;
// 		var clientY = event.clientY || event.touches[0].clientY;

// 		onMouseDownMouseX = clientX;
// 		onMouseDownMouseY = clientY;

// 		onMouseDownLon = lon;
// 		onMouseDownLat = lat;

// 		// update();

// 	}

// 	function onPointerMove(event) {

// 		if (isUserInteracting === true) {

// 			var clientX = event.clientX || event.touches[0].clientX;
// 			var clientY = event.clientY || event.touches[0].clientY;

// 			lon = (onMouseDownMouseX - clientX) * 0.1 + onMouseDownLon;
// 			lat = (clientY - onMouseDownMouseY) * 0.1 + onMouseDownLat;

// 			update();

// 		}

// 	}

// 	function onPointerUp() {

// 		isUserInteracting = false;

// 	}

// 	function onDocumentMouseWheel(event) {

// 		var fov = camera.fov + event.deltaY * 0.05;

// 		// camera.fov = THREE.Math.clamp(fov, 10, 75);

// 		camera.updateProjectionMatrix();

// 		update();

// 	}
// 	function update() {
// 		lat = 90 - Math.max(- 85, Math.min(85, lat));
// 		// phi = THREE.Math.degToRad(90 - lat);
// 		// theta = THREE.Math.degToRad(lon);

// 		phi = (lat * Math.PI) / 180;
// 		theta = (lon * Math.PI) / 180;

// 		let x = 500 * Math.sin(phi) * Math.cos(theta);
// 		let y = 500 * Math.cos(phi);
// 		let z = 500 * Math.sin(phi) * Math.sin(theta);

// 		camera.lookAt(x, y, z);

// 		scope.dispatchEvent(changeEvent);
// 		/*
// 		// distortion
// 		camera.position.copy( camera.target ).negate();
// 		*/
// 	}
// }
// PointerLockControls.prototype = Object.create(EventDispatcher.prototype);
// PointerLockControls.prototype.constructor = PointerLockControls;

// export { PointerLockControls }
