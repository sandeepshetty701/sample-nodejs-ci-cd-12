import * as THREE from 'three/build/three.module.js';

let hoverPosition = null;

export default class raycastUtils {

    constructor(canvas) {
        this.canvas = canvas;
        this.addEventListeners();
    }

    pick(objects, camera, filter = true) {
        const raycaster = new THREE.Raycaster();
        let pickedObject = undefined;
        raycaster.setFromCamera(hoverPosition, camera);
        const intersectedObjects = raycaster.intersectObjects(objects, true);
        if (filter) {
            if (intersectedObjects.length) {
                pickedObject = intersectedObjects[0].object;
                pickedObject.point = intersectedObjects[0].point;
                return pickedObject;
            } else {
                return null;
            }
        } else {
            return intersectedObjects;
        }
    }


    getCanvasRelativePosition(event) {
        const rect = this.canvas.getBoundingClientRect();
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top,
        };
    }

    getCanvasRelativePositionTouch(event) {
        const rect = this.canvas.getBoundingClientRect();
        return {
            x: event.touches[0].clientX - rect.left,
            y: event.touches[0].clientY - rect.top,
        };
    }

    setPickPosition(event) {
        const pos = this.getCanvasRelativePosition(event);
        let pickPosition = { x: 0, y: 0 };
        pickPosition.x = (pos.x / this.canvas.clientWidth) * 2 - 1;
        pickPosition.y = (pos.y / this.canvas.clientHeight) * -2 + 1;  // note we flip Y
        return pickPosition;
    }

    setTouchPosition(event) {
        const pos = this.getCanvasRelativePositionTouch(event);
        let pickPosition = { x: 0, y: 0 };
        pickPosition.x = (pos.x / this.canvas.clientWidth) * 2 - 1;
        pickPosition.y = (pos.y / this.canvas.clientHeight) * -2 + 1;  // note we flip Y
        return pickPosition;
    }

    canvasMouseMove(event) {
        hoverPosition = this.setPickPosition(event);
    }

    canvasTouchMove(event) {
        hoverPosition = this.setTouchPosition(event);
    }

    addEventListeners() {
        this.canvas.addEventListener('mousemove', (e) => {
            this.canvasMouseMove(e);
        });
        this.canvas.addEventListener('touchstart', (e) => {
            this.canvasTouchMove(e);
        });
        this.canvas.addEventListener('touchmove', (e) => {
            this.canvasTouchMove(e);
        });
    }
}