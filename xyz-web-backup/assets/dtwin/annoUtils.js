import * as THREE from 'three/build/three.module.js';

const annoMats = {
    annoMarker: {
        create: 0xff0000,
        select: 0x00ff00,
        drag: 0xff0000
    },
    annoEdge: {
        create: 0x00ff00,
        select: 0xff0000
    },
    annoTag: {
        create: 0x00ff00
    }
}

const annoMarkerSize = {
    panorama: 0.25,
    image: 0.01,
}

const annoTagSize = {
    panorama: 1,
    image: 0.05
}

export default class annoUtils {
    constructor(configObj) {
        this.canvas = configObj.canvas;
        this.scene = configObj.scene;
        this.camera = configObj.camera;
        this.eventService = configObj.eventService;
        this.raycastUtils = configObj.raycastUtils;
        this.mobileDevice = configObj.mobileDevice;

        this.pickEnabled = false;
        this.maxMarkersReached = false;
        this.mouseDown = false;
        this.mouseMove = false;

        this.selectedMarker = null;
        this.markerDragging = false;
        this.currentAnnotation = null;
        this.annoAddMode = false;
        this.userInteraction = this.mobileDevice;

        this.annoGrp = new THREE.Group();
        this.annoGrp.name = 'annotations';
        this.annoGrp.visible = this.userInteraction;
        this.scene.add(this.annoGrp);

        this.addEventListeners.bind(this)();
    }

    togglePointPick(cond) {
        this.pickEnabled = cond;
    }

    insertAnnotation(annoConfig) {
        this.maxMarkersReached = false;
        this.toggleAnnotation(this.currentAnnotation, false);
        this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: false});
        this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: false});
        this.annoAddMode = true;
        this.currentAnnotation = new THREE.Group();
        this.annoGrp.add(this.currentAnnotation);
        this.currentAnnotation.annoConfig = {
            type: annoConfig.type,
            assetType: annoConfig.assetType,
            maxMarkers: annoConfig.maxMarkers,
            minMarkers: annoConfig.minMarkers
        };
        this.togglePointPick(true);
        this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: true});
    }

    genAnnoFromJson(inObj) {
        let objProps = inObj.properties;
        let annotation = new THREE.Group();
        annotation.annoConfig = {
            type: objProps.type,
            assetType: objProps.assetType,
            id: inObj._id,
            title: objProps.title
        };
        let coordinates = inObj.geometry.coordinates;
        let prevMarker = null;
        coordinates.map((point,index) => {
            let curPosVec = new THREE.Vector3().fromArray(point);
            let marker = this.createMarker(curPosVec, objProps.assetType);
            annotation.add(marker);
            if(index > 0) {
                let line = this.createLine(prevMarker, marker, objProps.assetType);
                annotation.add(line);
            }
            prevMarker = marker;
        });

        if(annotation.annoConfig.type == 'polygon') {
            let annoCoords = this.getAnnoChildren(annotation, 'annoMarker');
            let annoCoordsLen = annoCoords.length;
            let line = this.createLine(annoCoords[annoCoordsLen-1], annoCoords[0], objProps.assetType);
            annotation.add(line);
        }

        let annoTag = this.createAnnoTag(new THREE.Vector3().fromArray(inObj.geometry.position), annotation.annoConfig.assetType);
        annotation.add(annoTag);
        this.toggleAnnotation(annotation, false);
        this.annoGrp.add(annotation);
    }

    async loadAnnotations(inAnnos, id = null) {
        inAnnos.forEach(anno => {
            this.genAnnoFromJson(anno);
        });
        if(id) {
            this.flyToAnnotation(id);
        }
    }

    discardAnnotation() {
        this.removeAnnotation(this.currentAnnotation, true);
        this.togglePointPick(false);
        this.currentAnnotation = null;
        this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: false});
        this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: false});
    }

    saveAnnotation() {
        let inAnno = this.currentAnnotation;
        if(this.annoAddMode) { // New Annotation
            this.annoAddMode = false;
            this.saveNewAnnotation(inAnno);
        } else { // Update Annotation
            this.updateEditedAnno(inAnno);
        }
        this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: false});
        this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: false});
    }

    async saveNewAnnotation(inAnno) {
        this.pickEnabled = false;
        let annoPos = this.getAnnoPosition(inAnno);
        let annoTag = this.createAnnoTag(annoPos, inAnno.annoConfig.assetType);
        inAnno.add(annoTag);
        this.toggleAnnotation(inAnno, false);

        let annoCoords = this.getAnnoChildren(inAnno, 'annoMarker');
        let coords = annoCoords.map(marker => {
            return marker.position.toArray();
        });

        let annoJson = {
            type: '3D',
            geometry: {
                coordinates: coords,
                position: annoPos.toArray()
            },
            properties: {
                type: inAnno.annoConfig.type,
                assetType: inAnno.annoConfig.assetType
            }
        };
        this.eventService.sendEvent('annotation', { type: 'save' , data: annoJson});
    }

    updateEditedAnno(inAnno) {
        this.pickEnabled = false;
        let tagPosition = this.getAnnoPosition(inAnno);
        let annoTag = this.getAnnoChildren(inAnno, 'annoTag');
        annoTag[0].position.set(tagPosition.x, tagPosition.y, tagPosition.z);
        this.toggleAnnotation(inAnno, false);

        let annoCoords = this.getAnnoChildren(inAnno, 'annoMarker');
        let coords = annoCoords.map(marker => {
            return marker.position.toArray();
        });

        let updatedAnno = {
            geometry: {
                coordinates: coords,
                position: tagPosition.toArray()
            }
        }
        this.eventService.sendEvent('annotation', { type: 'update' , data: 
                                                    {data: updatedAnno, id: inAnno.annoConfig.id}});
    }

    getAnnoPosition(inAnno) {
        let annoCoords = this.getAnnoChildren(inAnno, 'annoMarker');
        let annoCoordsLen = annoCoords.length;
        let xCoordsArr = annoCoords.map(marker => {
            return marker.position.x;
        });
        let yCoordsArr = annoCoords.map(marker => {
            return marker.position.y;
        });
        let zCoordsArr = annoCoords.map(marker => {
            return marker.position.z;
        });
        let calSum = (sum, num) => sum + num ;
        let posVec = new THREE.Vector3().fromArray(
            [xCoordsArr.reduce(calSum),yCoordsArr.reduce(calSum),zCoordsArr.reduce(calSum)].map(
                value => { return value/annoCoordsLen }
            )
        );
        return posVec;
    }

    createAnnoTag(position, type) {
        let markerSize = annoTagSize[type];
        var spriteMap = new THREE.TextureLoader().load('../../assets/dtwin3d/three/tag.png');
        var spriteMaterial = new THREE.SpriteMaterial( { map: spriteMap, color: 0xffffff, depthTest: false } );
        var marker = new THREE.Sprite( spriteMaterial );
        marker.renderType = 'annoTag';
        marker.scale.set(markerSize, markerSize, markerSize);
        marker.position.set(position.x, position.y, position.z);
        marker.visible = this.userInteraction;
        marker['onClick'] = (pickedObj, e) => {
            this.toggleAnnotation(this.currentAnnotation, false);
            this.toggleAnnotation(pickedObj.parent, true);
            this.currentAnnotation = pickedObj.parent;
            this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: true});
            this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: true});
            this.eventService.sendEvent('annotation', { type: 'select' , data: {id :this.currentAnnotation.annoConfig.id,
                                                                                title: this.currentAnnotation.annoConfig.title,
                                                                                position: {x: e.clientX, y: e.clientY}}});   
        }
        return marker;
    }

    removeAnnotations() {
        this.removeAnnotation(this.annoGrp.children);
    }

    removeAnnotation(inAnno, dbDelete = false) {
        if(Array.isArray(inAnno)) {
            for(let i=0;i<inAnno.length;i++) {
                this.removeAnnotationRaw(inAnno[i], dbDelete);
                i = i-1;
            }
        } else {
            this.removeAnnotationRaw(inAnno, dbDelete);
        }
    }

    removeAnnotationRaw(inAnno, dbDelete) {
        inAnno.geometry && inAnno.geometry.dispose();
        inAnno.material && inAnno.material.dispose();
        this.annoGrp.remove(inAnno);
        if(inAnno.annoConfig.id && dbDelete) {
            this.eventService.sendEvent('annotation', { type: 'delete' , data: inAnno.annoConfig.id});
        }
    }

    toggleAnnotation(inAnno, inCond) {
        if(inAnno) {
            let annoMarkers = this.getAnnoChildren(inAnno, 'annoMarker');
            annoMarkers.map(marker => marker.visible = inCond);
            let annoEdges = this.getAnnoChildren(inAnno, 'annoEdge');
            annoEdges.map(edge => edge.visible = inCond);
            let annoTag = this.getAnnoChildren(inAnno, 'annoTag');
            annoTag.map(tag => tag.visible = !inCond);
            this.eventService.sendEvent('render');
        }
    }

    getAnnoChildren(inAnnoGrp, inType) {
        let children = inAnnoGrp.children;
        let filtered = children.filter(child => child.renderType === inType);
        return filtered;
    }

    createMarker(position, type) {
        let markerSize = annoMarkerSize[type];
        var geometry = new THREE.SphereBufferGeometry(markerSize, 10, 10);
        var material = new THREE.MeshBasicMaterial({ color: annoMats.annoMarker.create });
        var marker = new THREE.Mesh(geometry, material);
        marker.renderType = 'annoMarker';
        marker.position.set(position.x, position.y, position.z);
        return marker;
    }

    addMarker(position) {
        console.log('Marker Add');
        let marker = this.createMarker(position, this.currentAnnotation.annoConfig.assetType);
        this.currentAnnotation.add(marker);
        let annoCoords = this.getAnnoChildren(this.currentAnnotation, 'annoMarker');
        let annoCoordsLen = annoCoords.length;

        let removeLastEdge = () => {
            let annoEdges = this.getAnnoChildren(this.currentAnnotation, 'annoEdge');
            let lastCoord = annoCoords[annoCoordsLen-2];
            let lastEdge = annoEdges.filter(edge => edge.markerOne == lastCoord.id)[0];
            lastEdge.geometry && lastEdge.geometry.dispose();
            lastEdge.material && lastEdge.material.dispose();
            this.currentAnnotation.remove(lastEdge);
        }

        let createLine = (pos1, pos2) => {
            let line = this.createLine(pos1, pos2, this.currentAnnotation.annoConfig.assetType);
            this.currentAnnotation.add(line);
        }

        if (this.currentAnnotation.annoConfig.type === 'polygon') {
            if (annoCoordsLen > 3) {
                removeLastEdge();
                createLine(annoCoords[annoCoordsLen - 2], annoCoords[annoCoordsLen - 1]);
                createLine(annoCoords[annoCoordsLen - 1], annoCoords[0]);
            } else if (annoCoordsLen > 2) {
                createLine(annoCoords[annoCoordsLen - 2], annoCoords[annoCoordsLen - 1]);
                createLine(annoCoords[annoCoordsLen - 1], annoCoords[0]);
            } else if (annoCoordsLen > 1) {
                createLine(annoCoords[annoCoordsLen - 2], annoCoords[annoCoordsLen - 1]);
            }
        } else if (annoCoordsLen > 1) {
            createLine(annoCoords[annoCoordsLen - 2], annoCoords[annoCoordsLen - 1]);
        }

        if (annoCoordsLen == this.currentAnnotation.annoConfig.maxMarkers) {
            // this.togglePointPick(false);
            this.maxMarkersReached = true;
        }

        if(annoCoordsLen == this.currentAnnotation.annoConfig.minMarkers) {
            this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: true});
        }
        this.eventService.sendEvent('render');
    }

    removeFromScene(obj) {
        obj.geometry && obj.geometry.dispose();
        obj.material && obj.material.dispose();
        this.scene.remove(obj);
    }

    createLine(pointOne, pointTwo, assetType) {
        if(assetType == 'image') {
            return this.createCylinderGeo(pointOne, pointTwo);
        } else {
            return this.createLineGeo(pointOne, pointTwo);
        }
    }

    createCylinderGeo(pointOne, pointTwo) {
        var direction = new THREE.Vector3().subVectors( pointTwo.position, pointOne.position );
        var orientation = new THREE.Matrix4();
        /* THREE.Object3D().up (=Y) default orientation for all objects */
        orientation.lookAt(pointOne.position, pointTwo.position, new THREE.Object3D().up);
        orientation.multiply(new THREE.Matrix4().set(1,0,0,0,0,0,1,0,0,-1,0,0,0,0,0,1));
        var edgeGeometry = new THREE.CylinderGeometry( 0.0025, 0.0025, direction.length(), 8, 1);
        let material = new THREE.LineBasicMaterial( {color: annoMats.annoEdge.create});
        var edge = new THREE.Mesh( edgeGeometry, material );
        edge.applyMatrix(orientation);
        let pos = new THREE.Vector3().addVectors( pointOne.position, direction.multiplyScalar(0.5) );
        edge.position.set(pos.x,pos.y,pos.z);
        edge.renderType = 'annoEdge';
        edge.markerOne = pointOne.id;
        edge.markerTwo = pointTwo.id; 
        return edge;
    }

    createLineGeo(pointOne, pointTwo) {
        let material = new THREE.LineBasicMaterial( {color: annoMats.annoEdge.create, linewidth: 2});
        let vertices = new Float32Array(this.getArrayFromVector([pointOne.position, pointTwo.position]));
        var geometry = new THREE.BufferGeometry();
        geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
        let line = new THREE.Line(geometry, material);
        line.renderType = 'annoEdge';
        line.markerOne = pointOne.id;
        line.markerTwo = pointTwo.id;
        return line;
    }

    getArrayFromVector(vectors) {
        if (Array.isArray(vectors)) {
            let coordsArray = vectors.map(vector => {
                return vector.toArray();
            });
            return coordsArray.flat();
        } else {
            return vectors.toArray();
        }
    }

    onCanvasClick(e, callback = null) {
        if (this.mouseMove) {
            this.mouseMove = false;
        } else {
            if(this.pickEnabled) {
                let pickedObj = this.raycastUtils.pick(this.scene.children.filter(child => child.name != 'annotations'), this.camera);
                pickedObj && !this.maxMarkersReached && this.addMarker(pickedObj.point);
            } 
            // else {
                // let pickedObj = this.raycastUtils.pick(this.scene.children, this.camera);
                // if(pickedObj && pickedObj.renderType === 'annoTag') {
                //     this.toggleAnnotation(this.currentAnnotation, false);
                //     this.toggleAnnotation(pickedObj.parent, true);
                //     this.currentAnnotation = pickedObj.parent;
                //     this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: true});
                //     this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: true});
                //     this.eventService.sendEvent('annotation', { type: 'select' , data: {id :this.currentAnnotation.annoConfig.id,
                //                                                                         title: this.currentAnnotation.annoConfig.title,
                //                                                                         position: {x: e.clientX, y: e.clientY}}});
                // } 
                else if(callback) {
                    callback(e);
                }
            // }
        }
    }

    onCanvasMouseDown(e) {
        this.mouseDown = true;
        this.currentAnnotation && this.checkForMarkerDrag.bind(this)(e);
    }

    onCanvasMouseUp(e) {
        this.mouseDown = false;
        if(this.selectedMarker) {
            this.resetAnnotation(this.selectedMarker.parent);
            this.eventService.sendEvent('controls', { type: 'toggle' });
        }
        this.selectedMarker = null;
    }

    onCanvasMouseMove(e) {
        if (this.mouseDown) {
            this.mouseMove = true;
            if (this.selectedMarker && !this.markerDragging) {
                // console.log('Marker Drag');
                this.updateMarkerPosition(e);
            }
        }
        this.eventService.sendEvent('render');
    }

    checkForMarkerDrag(e) {
       let pickedObj = this.raycastUtils.pick(this.currentAnnotation.children.filter(child => child.renderType=='annoMarker'), this.camera);
        if (pickedObj) {
            console.log('Enable Marker Drag');
            this.selectedMarker = pickedObj;
            this.selectedMarker.material.color.set(annoMats.annoMarker.drag);
            this.selectAnnotation(this.selectedMarker.parent);
            this.eventService.sendEvent('controls', { type: 'toggle' });
        }
    }

    selectAnnotation(inAnno) {
        this.currentAnnotation = inAnno;
        let annoMarkers = this.getAnnoChildren(inAnno, 'annoMarker');
        let annoEdges = this.getAnnoChildren(inAnno, 'annoEdge');
        annoMarkers.map(marker => {
            if(marker.id != this.selectedMarker.id) {
                marker.material.color.set(annoMats.annoMarker.select);
            }
        });
        annoEdges.map(edge => {
            edge.material.color.set(annoMats.annoEdge.select);
        });
    }

    resetAnnotation(inAnno) {
        let annoMarkers = this.getAnnoChildren(inAnno, 'annoMarker');
        let annoEdges = this.getAnnoChildren(inAnno, 'annoEdge');

        annoMarkers.map(marker => {
            marker.material.color.set(annoMats.annoMarker.create);
        });
        annoEdges.map(edge => {
            edge.material.color.set(annoMats.annoEdge.create);
        });
    }

    updateMarkerPosition(e) {
        let hoveredObjs = this.raycastUtils.pick(this.scene.children.filter(child => child.name != 'annotations'), this.camera, false);
        if (hoveredObjs.length) {
            this.markerDragging = true;
            this.selectedMarker.position.copy(hoveredObjs[0].point);
            this.currentAnnotation.annoConfig.type != 'point' && this.updateAnnotation(this.selectedMarker);
            this.markerDragging = false;
        } else {
            this.markerDragging = false;
        }
    }

    updateAnnotation(marker) {
        let inAnno = marker.parent;
        let markerID = marker.id;
        let annoCoords = this.getAnnoChildren(inAnno, 'annoMarker');
        let annoEdges = this.getAnnoChildren(inAnno, 'annoEdge');
        let removeEdges = (edges) => {
            edges.forEach(edge => {
                edge.geometry && edge.geometry.dispose();
                edge.material && edge.material.dispose();
                this.currentAnnotation.remove(edge);
            });
        }
        let updateEdge = (edge, pos1, pos2) => {
            edge.geometry.attributes.position.copyArray(
                this.getArrayFromVector([pos1, pos2]));
            edge.geometry.attributes.position.needsUpdate = true;
            edge.material.color.set(annoMats.annoEdge.create);
        }

        let edge1 = annoEdges.filter(edge => edge.markerOne == markerID)[0];
        let edge2 = annoEdges.filter(edge => edge.markerTwo == markerID)[0];
        if(edge1 && edge2){
            let point1 = annoCoords.filter(coord => coord.id == edge1.markerTwo)[0];
            let point2 = annoCoords.filter(coord => coord.id == edge2.markerOne)[0];
            if(this.currentAnnotation.annoConfig.assetType == 'image') {
                removeEdges([edge1, edge2]);
                let newEdge1 = this.createLine(marker, point1, 'image');
                this.currentAnnotation.add(newEdge1);
                let newEdge2 = this.createLine(point2, marker, 'image');
                this.currentAnnotation.add(newEdge2);
            } else {
                updateEdge(edge1, marker.position, point1.position);
                updateEdge(edge2, point2.position, marker.position);
            }
        } else if(edge1) {
            let point1 = annoCoords.filter(coord => coord.id == edge1.markerTwo)[0];
            if(this.currentAnnotation.annoConfig.assetType == 'image') {
                removeEdges([edge1]);
                let newEdge1 = this.createLine(marker, point1, 'image');
                this.currentAnnotation.add(newEdge1);
            } else {
                updateEdge(edge1, marker.position, point1.position);
            }
        } else if(edge2) {
            let point2 = annoCoords.filter(coord => coord.id == edge2.markerOne)[0];
            if(this.currentAnnotation.annoConfig.assetType == 'image') {
                removeEdges([edge2]);
                let newEdge2 = this.createLine(point2, marker, 'image');
                this.currentAnnotation.add(newEdge2);
            } else {
                updateEdge(edge2, point2.position, marker.position);
            }
        }
    }

    flyToAnnotation(id) {
        let selectedAnno = this.annoGrp.children.filter(anno => {
            if(anno.annoConfig.id && anno.annoConfig.id === id) {
                return true
            } else {
                return false
            }
        });
        if(selectedAnno.length) {
            this.toggleAnnotation(this.currentAnnotation, false);
            this.toggleAnnotation(selectedAnno[0], true);
            this.currentAnnotation = selectedAnno[0];
            this.eventService.sendEvent('toolBar', { type: 'cancelBut' , data: true});
            this.eventService.sendEvent('toolBar', { type: 'saveBut' , data: true});
            let annoTagPos = this.getAnnoChildren(this.currentAnnotation, 'annoTag')[0].position;
            console.log(annoTagPos);
            this.camera.lookAt(annoTagPos);
            this.eventService.sendEvent('render',{type: 'camera', data: this.camera.clone()});
        }
    }

    annosInteraction(condition) {
        this.userInteraction = condition;
        this.annoGrp.visible = condition;
    }

    addEventListeners() {

        if(this.mobileDevice) {
            this.canvas.addEventListener('touchstart', (e) => {
                this.onCanvasClick.bind(this)(e, this.onCanvasMouseDown.bind(this));
            });
            this.canvas.addEventListener('touchmove', this.onCanvasMouseMove.bind(this));
            this.canvas.addEventListener('touchend', this.onCanvasMouseUp.bind(this));
        } else {
            this.canvas.addEventListener('click', this.onCanvasClick.bind(this));
            this.canvas.addEventListener('mousemove', this.onCanvasMouseMove.bind(this));
            this.canvas.addEventListener('mousedown', this.onCanvasMouseDown.bind(this));
            this.canvas.addEventListener('mouseup', this.onCanvasMouseUp.bind(this));
        }
    }
}
