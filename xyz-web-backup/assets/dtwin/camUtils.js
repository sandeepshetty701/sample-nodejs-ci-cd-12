import * as THREE from 'three/build/three.module.js';

export default class CamUtils {
    constructor(camera, tween, eventService) {
        this.camera = camera;
        this.TWEEN = tween;
        this.eventService = eventService;
        this.prevCameraObj = { 'position': null, 'target': null };
    }

    storePrevCamVals(camera) {
        this.prevCameraObj.position = camera.position;
        let camDir = new THREE.Vector3();
        camera.getWorldDirection(camDir);
        camDir.normalize();
        let camTarget = new THREE.Vector3();
        camTarget.addVectors(camera.position, camDir.multiplyScalar(20));
        this.prevCameraObj.target = camTarget;
    }

    setPrevCamVals() {
        let pos = this.prevCameraObj.position;
        let target = this.prevCameraObj.target;
        this.setCamVals({ 'position': pos, 'target': target, 'fov': 60 });
    }

    setCamVals(camObj) {
        let curCam = this.camera.clone();
        this.storePrevCamVals(curCam);
        if (camObj.position) {
            let pos = camObj.position;
            let curCamPos = curCam.position;
            this.tweenCamPos({ x: curCamPos.x, y: curCamPos.y, z: curCamPos.z }, { x: pos.x, y: pos.y, z: pos.z }, 500, camObj.target);
        }
        if (camObj.fov) {
            this.camera.fov = camObj.fov;
            this.camera.updateProjectionMatrix();
            this.eventService.sendEvent('render', { type: 'camera', data: this.camera.clone() });
        }
        this.dispatchEvent({ 'type': 'render' });
    }

    tweenCamPos(inObj, outObj, duration, target, callback=null) {
        let tween = new this.TWEEN.Tween(inObj).to(outObj, duration);
        tween.easing(this.TWEEN.Easing.Sinusoidal.InOut);
        let onUpdate = () => {
            this.camera.position.x = inObj.x;
            this.camera.position.y = inObj.y;
            this.camera.position.z = inObj.z;
            this.eventService.sendEvent('render', { type: 'camera', data: this.camera.clone() });
            this.dispatchEvent({ 'type': 'render' });
        };
        let onComplete = () => {
            if (target) {
                this.camera.lookAt(target);
                this.eventService.sendEvent('render', { type: 'camera', data: this.camera.clone() });
            }
            if(callback) {
                callback();
            }
        }
        tween.onUpdate(onUpdate);
        tween.onComplete(onComplete);
        tween.start();
        this.dispatchEvent({ 'type': 'render' });
    }

    tweenCamPitch(delta) {
        let euler = new THREE.Euler(0, 0, 0, 'ZYX');
        euler.setFromQuaternion(this.camera.quaternion);
        let posSrc = { x: euler.x };
        let posDes = { x: euler.x + delta };
        let tween = new this.TWEEN.Tween(posSrc).to(posDes, 500);
        let onUpdate = () => {
            euler.x = posSrc.x;
            this.camera.quaternion.setFromEuler(euler);
            this.dispatchEvent({ 'type': 'render' });
        };
        tween.onUpdate(onUpdate);
        tween.start();
        this.dispatchEvent({ 'type': 'render' });
    }

    setCamPitch(radians) {
        let euler = new THREE.Euler(0, 0, 0, 'ZYX');
        euler.x = radians;
        this.camera.quaternion.setFromEuler(euler);
        this.dispatchEvent({ 'type': 'render' });
    }

    tweenCamYaw(delta) {
        let euler = new THREE.Euler(0, 0, 0, 'ZYX');
        euler.setFromQuaternion(this.camera.quaternion);
        let posSrc = { z: euler.z };
        let posDes = { z: euler.z + delta };
        let tween = new this.TWEEN.Tween(posSrc).to(posDes, 500);
        let onUpdate = () => {
            euler.z = posSrc.z;
            this.camera.quaternion.setFromEuler(euler);
            this.dispatchEvent({ 'type': 'render' });
        };
        tween.onUpdate(onUpdate);
        tween.start();
        this.dispatchEvent({ 'type': 'render' });
    }

    syncCamera(cam) {
        this.camera.copy(cam, true);
        this.camera.updateProjectionMatrix();
        this.dispatchEvent({ 'type': 'render' });
    }

    setCamTarget(target) {
        var camDir = new THREE.Vector3( 0, 0, - 1 );
        camDir.applyQuaternion( this.camera.quaternion );
        var startPos = this.camera.position.clone();
        var camTarget = new THREE.Vector3();
        camTarget.addVectors ( startPos, camDir.multiplyScalar( 1 ) );

        let tween = new this.TWEEN.Tween(camTarget).to(target, 250);
        let onUpdate = () => {
            this.camera.lookAt(new THREE.Vector3(camTarget.x, camTarget.y, camTarget.z));
            this.dispatchEvent({ 'type': 'render' });
        };
        tween.onUpdate(onUpdate);
        tween.start();
        this.dispatchEvent({ 'type': 'render' });
    }

    calBBox(asset) {
        let box;
        if (asset.renderType == 'mesh') {
            box = new THREE.Box3().setFromObject(asset);
            return box
        } else if (asset.renderType == 'pointcloud') {
            asset.updateMatrixWorld(true);
            let tightBB = asset.pcoGeometry.tightBoundingBox;
            let transform = asset.matrixWorld;
            box = this.computeTransformedBoundingBox(tightBB, transform);
            return box
        } else {
            return null
        }
    }

    calBiggerBB(box1, box2) {
        let box1minarr = box1.min.toArray();
        let box2minarr = box2.min.toArray();
        let box1maxarr = box1.max.toArray();
        let box2maxarr = box2.max.toArray();
        let bbMin = box1minarr.map((e, i) => {
            if (box1minarr[i] > box2minarr[i]) {
                return box2minarr[i];
            } else {
                return e;
            }
        });
        let bbMax = box1maxarr.map((e, i) => {
            if (box1maxarr[i] > box2maxarr[i]) {
                return e;
            } else {
                return box2maxarr[i];
            }
        });
        return { min: new THREE.Vector3().fromArray(bbMin), max: new THREE.Vector3().fromArray(bbMax) }
    }

    fitCameraToAsset(assets, controls, animateCB) {
        if (Array.isArray(assets)) {
            let box = new THREE.Box3();
            box.set(new THREE.Vector3(Infinity, Infinity, Infinity), new THREE.Vector3(-Infinity, -Infinity, -Infinity));
            assets.forEach(asset => {
                // if (asset.children.length) {
                //     asset.children.forEach(child => {
                //         let curBox = this.calBBox(child);
                //         if (curBox) {
                //             let bbs = this.calBiggerBB(box, curBox);
                //             box.set(bbs.min, bbs.max);
                //         }
                //     });
                // } else if (asset) {
                //     let curBox = this.calBBox(asset);
                //     if (curBox) {
                //         let bbs = this.calBiggerBB(box, curBox);
                //         box.set(bbs.min, bbs.max);
                //     }
                // }
                let curBox = this.calBBox(asset);
                if (curBox) {
                    let bbs = this.calBiggerBB(box, curBox);
                    box.set(bbs.min, bbs.max);
                }
            });
            if (box) {
                this.setCamToAssets(box, controls, animateCB);
            }
        } else if (assets) {
            let box = this.calBBox(assets);
            // if (assets.children.length) {
            //     assets.children.forEach(child => {
            //         let curBox = this.calBBox(child);
            //         if (curBox) {
            //             let bbs = this.calBiggerBB(box, curBox);
            //             box.set(bbs.min, bbs.max);
            //         }
            //     });
            // }
            // else if(assets) {
            //     let curBox = this.calBBox(assets);
            //     if(curBox) {
            //         let bbs = this.calBiggerBB(box,curBox);
            //         box.set(bbs.min,bbs.max);
            //     }
            // }
            if (box) {
                this.setCamToAssets(box, controls, animateCB);
            }
        }
    }

    setCamToAssets(box, controls, animateCB) {
        const boxSize = box.getSize(new THREE.Vector3()).length();
        const boxCenter = box.getCenter(new THREE.Vector3());
        this.frameArea(boxSize, boxSize, boxCenter, this.camera, animateCB);
        // update the Trackball controls to handle the new size
        (controls.name === 'OrbitControls') && this.updateControls(boxSize, boxCenter, controls);
        // this.dispatchEvent({ 'type': 'render'});
        this.eventService.sendEvent('render');
    }

    updateControls(boxSize, boxCenter, controls) {
        controls.maxDistance = boxSize * 10;
        controls.target.copy(boxCenter);
        controls.update();
        this.eventService.sendEvent('render', { type: 'controls', data: { maxDistance: controls.maxDistance, target: controls.target } });
    }

    frameArea(sizeToFitOnScreen, boxSize, boxCenter, camera, animateCB) {
        // const halfSizeToFitOnScreen = sizeToFitOnScreen * 0.5;
        // const halfFovY = THREE.Math.degToRad(camera.fov * .5);
        const halfSizeToFitOnScreen = sizeToFitOnScreen * 1;
        const halfFovY = THREE.Math.degToRad(camera.fov);
        const distance = halfSizeToFitOnScreen / Math.tan(halfFovY);

        // const direction = (new THREE.Vector3())
        //     .subVectors(camera.position, boxCenter)
        //     .multiply(new THREE.Vector3(1, 0, 1))
        //     .normalize();

        // let curCamPos = camera.position.clone();
        // let pos = direction.multiplyScalar(distance).add(boxCenter);
        // this.tweenCamPos({ x: curCamPos.x, y: curCamPos.y, z: curCamPos.z}, { x: pos.x, y: pos.y, z: pos.z  + (boxSize/4)}, 500, boxCenter);


        // Will Go To Top Slant View 
        let curCamPos = camera.position.clone();
        if(animateCB) {
            this.tweenCamPos({ x: curCamPos.x, y: curCamPos.y, z: curCamPos.z}, { x: (boxCenter.x + boxSize/2), y: boxCenter.y, z: boxCenter.z  + boxSize/2}, 500, boxCenter, animateCB);
        } else {
            this.tweenCamPos({ x: curCamPos.x, y: curCamPos.y, z: curCamPos.z}, { x: (boxCenter.x + boxSize/2), y: boxCenter.y, z: boxCenter.z  + boxSize/2}, 500, boxCenter);
        }
    }

    computeTransformedBoundingBox(box, transform) {
        let vertices = [
			new THREE.Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.max.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.min.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.max.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.max.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.min.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.max.y, box.max.z).applyMatrix4(transform)
		];

		let boundingBox = new THREE.Box3();
		boundingBox.setFromPoints( vertices );

		return boundingBox;
    }
}