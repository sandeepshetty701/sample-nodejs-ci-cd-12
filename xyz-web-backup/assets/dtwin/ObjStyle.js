export default class DT3DStyle {
    constructor(options) {
        this.color = options.color;
        this.opacity = options.opacity;
        this.visibility = options.visibility;
        this.material = options.material;
    }
}